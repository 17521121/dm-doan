
import numpy as np
import math
import argparse
import pickle
from sklearn import preprocessing

parser = argparse.ArgumentParser(description='Some arguments need passing')

parser.add_argument("--file", default="", help="Path to file")
parser.add_argument(
    "--model_name", default="svm_tfidfscale_0.25.sav", help="Pretrained model")


args = parser.parse_args()

pretrained_model = pickle.load(open(args.model_name, 'rb'))
pretrained_model_property = args.model_name.split("_")


header = []
with open("header.csv", "r") as input_header:
    for i in input_header.read().split(","):
        header.append(i)

idf = []
with open("idf_weights.csv", "r") as idf_reader:
    for i in idf_reader.read().split(","):
        idf.append(float(i))


def tf_compute(term, document):
    return 1 + math.log2(document.count(term))


def input_to_vector(str_input):
    x = []
    for i in str_input:
        terms = i.lower().split(" ")
        x_test = []
        if pretrained_model_property[1] == "tfidf" or pretrained_model_property[1] == "tfidfscale": 
            x_test = np.zeros(len(header), dtype="float16")
            for i in terms:
                try:
                    index = header.index(i)
                    x_test[index] = tf_compute(val, terms) * idf[index]
                except:
                    pass

        elif pretrained_model_property[1] == "onehot":
            x_test = np.zeros(len(header), dtype="int8")
            for i in terms:
                try:
                    index = header.index(i)
                    x_test[index] = 1
                except:
                    pass

        x.append(x_test)

    if pretrained_model_property[1] == "tfidfscale":
        x = preprocessing.normalize(x)

    return x


# Counter
it = 0
if(args.file == ""):
    args.file = "input.txt"

with open(args.file, "r") as str_reader:
    x_test = []
    for str_input in str_reader.read().splitlines():
        x_test.append(str_input)
    x_test = input_to_vector(x_test)

    result = pretrained_model.predict(x_test)
    for i in result:
        if i == 1:
            print("%d. Quang cao phu hop" % it)
        else:
            print("%d. Quang cao khong phu hop" % it)
        it = it + 1
