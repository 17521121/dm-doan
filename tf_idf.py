import numpy as np
import math

data = []
label = []
with open("farm-data/farm-ads", "r") as ip:
    for line in ip.read().splitlines():
        term = line.lower().split(" ")
        data.append(term[1:])
        label.append(term[0])

size_of_data = len(data)


with open("label.csv", "w+") as lb:
    lb.write(",".join(label))

header = []
for document in data:
    for term in document:
        if term not in header:
            header.append(term)

with open("header.csv", "w+") as header_writer:
    header_writer.write(",".join(header))

print("Finish load dataset")


def idf_compute():
    idf_list = []

    for term in header:
        count = 0
        for document in data:
            if term in document:
                count += 1
        idf_list.append(1 + math.log2(size_of_data/count))

    return idf_list


def tf_compute(term, document):
    return 1 + math.log2(document.count(term))


def tf_idf_compute():

    idf_list = idf_compute()
    with open("idf_weights.csv", "w+") as idf_writer:
        idf_writer.write(",".join([str(round(i, 3)) for i in idf_list]))
    print("Finish compute and write idf")

    # idf_list = []
    # with open("idf_weights.csv", "w+") as idf_writer:
    #     idf_list = idf_writer.read().split(",")
    
    tf_idf = []
    for row in data:
        arr = []
        for term in row:
            try:
                index = header.index(term)
                tf_idf_term = tf_compute(term, row) * idf_list[index]
                arr.append(str.format("{0}:{1}", index, round(tf_idf_term, 3)))
            except:
                pass
        tf_idf.append(",".join(arr))

    with open("tf_idf_sparse.csv", "w+") as tf_idf_writer:
        tf_idf_writer.write("\n".join(tf_idf))


tf_idf_compute()
