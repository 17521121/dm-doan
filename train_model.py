
import numpy as np
import pickle
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn import preprocessing
import argparse

parser = argparse.ArgumentParser(description='Some arguments need passing')

parser.add_argument("--test_size", default="0.25",
                    help="Porpotion of train and test size")

parser.add_argument("--converter", default="onehot",
                    help="Type of convert dataset: onehot, tfidf and tfidfscale")

parser.add_argument("--model", default="lr",
                    help="Logistic regression: lr, Knn: knn, SVM: svm")


args = parser.parse_args()

args.test_size = round(float(args.test_size), 2)
model_name = args.model + "_" + args.converter + \
    "_" + str(args.test_size) + ".sav"

label = []
with open("label.csv", "r") as lb:
    for i in lb.read().split(","):
        label.append(int(i))

header = []
with open("header.csv", "r") as input_header:
    for i in input_header.read().split(","):
        header.append(i)


term_size = len(header)
dbs_size = len(label)

# Onehot
if args.converter == "onehot":
    dataset = np.zeros((dbs_size, term_size), dtype="int8")
    with open("onehot_sparse.csv", "r") as onehot_reader:
        for index, row in enumerate(onehot_reader.read().splitlines()):
            for j in row.split(","):
                pos = int(j.split(":")[0])
                dataset[index, pos] = 1

#tf_idf
elif args.converter == "tfidf" or args.converter == "tfidfscale":
    dataset = np.zeros((dbs_size, term_size), dtype="float16")
    with open("tf_idf_sparse.csv", "r") as tf_idf_reader:
        for index, row in enumerate(tf_idf_reader.read().splitlines()):
            for j in row.split(","):
                pos, val = j.split(":")
                dataset[index, int(pos)] = float(val)

if args.converter == "tfidfscale":
    dataset =  preprocessing.normalize(dataset)

x_train, x_test, y_train, y_test = train_test_split(dataset, label,
                                                    test_size=args.test_size,
                                                    random_state=None,  # use np.random
                                                    shuffle=True)


def precision(predict, label):
    size_evaluation = len(predict)
    TP = 0
    for i in range(size_evaluation):
        if(predict[i] == label[i] == 1):
            TP += 1
    return TP/predict.count(1)


def recall(predict, label):
    size_evaluation = len(predict)
    FN = 0
    TP = 0
    for i in range(size_evaluation):
        if(predict[i] == label[i] == 1):
            TP += 1
        elif(predict[i] == -1 and label[i] == 1):
            FN += 1

    return TP/(TP+FN)


def f_measure(precision_score, recall_score):
    return 2*(precision_score*recall_score)/(precision_score + recall_score)


def evaluation(model):
    current_test_size = len(y_test)
    chart_ite = []
    chart_f1 = []
    chart_p = []
    chart_r = []
    for i in range(int(current_test_size/5), current_test_size, int(current_test_size/5)):
        x = x_test[0:i]
        y = y_test[0:i]
        label_of_predict = model.predict(x)
        label_of_predict = list(label_of_predict)
        precision_score = precision(label_of_predict, y)
        recall_score = recall(label_of_predict, y)
        f_measure_score = f_measure(precision_score, recall_score)
        print("Size of tests: %d, precision: %.3f, recall: %.3f, F1: %.3f" %
              (i, precision_score, recall_score, f_measure_score))

        chart_ite.append(i)
        chart_f1.append(f_measure_score)
        chart_p.append(precision_score)
        chart_r.append(recall_score)

    df = pd.DataFrame({'x': chart_ite, 'F_measure': chart_f1,
                       'Precision': chart_p, 'Recall': chart_r})

    plt.plot('x', 'F_measure', data=df, marker='o',
             markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4)
    plt.plot('x', 'Precision', data=df, marker='',
             color='olive', linewidth=2, )
    plt.plot('x', 'Recall', data=df, marker='',
             color='olive', linewidth=2, linestyle='dashed')
    plt.legend()

    plt.xlabel('Size of tests')
    plt.ylabel('Percentage')
    # plt.title('Experiment evaluation')
    plt.grid(True)
    plt.tight_layout()
    plt.show()


if args.model == "lr":
    from sklearn.linear_model import LogisticRegression
    model = LogisticRegression(solver='liblinear')

elif args.model == "svm":
    from sklearn.linear_model import SGDClassifier
    model = SGDClassifier(loss="hinge", penalty="l2")

elif args.model == "knn":
    from sklearn.neighbors import KNeighborsClassifier
    model = KNeighborsClassifier(n_neighbors=3)


model.fit(x_train, y_train)
pickle.dump(model, open(model_name, 'wb'))
evaluation(model)
