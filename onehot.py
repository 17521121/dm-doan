import numpy as np

label = []
with open("label.csv", "r") as lb:
    for i in lb.read().split(","):
        label.append(int(i))

header = []
with open("header.csv", "r") as input_header:
    for i in input_header.read().split(","):
        header.append(i)


term_size = len(header)
dbs_size = len(label)


data = [] 
with open("farm-data/farm-ads", "r") as db_from_file:
    for i in db_from_file.read().splitlines():
        data.append(i.split(" "))

for i in range(len(data)):
    temp = np.zeros(term_size, dtype = "int8")
    for j in range(1, len(data[i])):
        temp[header.index(data[i][j])] = 1
    data[i] = temp

with open("onehot_sparse.csv", "w+") as onehot_writer:
    for i in data:
        for index, val in enumerate(i):
            if val:
                onehot_writer.write("%d:1," % index)
        onehot_writer.seek(onehot_writer.tell()-1)
        onehot_writer.write("\n")
 
