﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farm_Ads
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string cmdOutput;
        string pretrained_model = "lr_onehot_0.25.sav";
        private void pythonCmd(string file_path, string args)
        {
            ProcessStartInfo p = new ProcessStartInfo();
            p.FileName = "cmd.exe";

            p.Arguments = string.Format("/c python {0} {1}", file_path, args);
            p.UseShellExecute = false;
            p.RedirectStandardOutput = true;
            p.CreateNoWindow = true;

            Process process = new Process();
            process.StartInfo = p;
            process.Start();

            using (StreamReader reader = process.StandardOutput)
            {
                cmdOutput = reader.ReadToEnd();
                //MessageBoxResult msg = MessageBox.Show(cmdOutput, "Experiment");

            }
            process.Close();
        }

        private void Train_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            string model = "";
            string converter = "";

            if (lr.IsChecked == true)
            {
                model = "lr";
            }
            else if (svm.IsChecked == true)
            {
                model = "svm";
            }
            else if (knn.IsChecked == true)
            {
                model = "knn";
            }

            if (onehot.IsChecked == true)
            {
                converter = "onehot";
            }
            else if (tfidf.IsChecked == true)
            {
                converter = "tfidf";
            }
            else if (tfidfscale.IsChecked == true)
            {
                converter = "tfidfscale";
            }

            string args = string.Format(" --test_size {0}  --converter {1}  --model {2}", test_size.Value, converter, model);
            pythonCmd("train_model.py", args);
            this.Cursor = Cursors.Arrow;
            cmdOutput += "\n\n Model save at: " + model + "_" + converter + "_" + test_size.Value + ".sav";
            MessageBoxResult msg = MessageBox.Show(cmdOutput, "Experiment");

        }

        private void Load(object sender, RoutedEventArgs e)
        {
            try
            {
                tbOutput.Text = "";
                if (rFromKeyboard.IsChecked == true)
                {
                    tbInputFromKeyboard.IsEnabled = true;
                    tbInputFromKeyboard.Focus();
                }
                else
                {
                    tbInputFromKeyboard.IsEnabled = false;
                    var ofd = new Microsoft.Win32.OpenFileDialog
                    {
                        InitialDirectory = Directory.GetCurrentDirectory(),
                        RestoreDirectory = true,
                        Title = "File contains test data",
                        Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
                    };
                    var result = ofd.ShowDialog();
                    if (result == false) return;
                    string filePath = ofd.FileName;

                    string args = " --file \"" + filePath + "\" --model \"" + pretrained_model + "\"";

                    pythonCmd("predict.py", args);
                    tbOutput.Text = cmdOutput;

                }
            }
            catch (Exception err)
            {

            }

        }

        private void BtPredict_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Wait;
                if (tbInputFromKeyboard.Text == "")
                    return;


                using (StreamWriter outputFile = new StreamWriter("input.txt"))
                {
                    outputFile.Write(tbInputFromKeyboard.Text);
                }

                string args = " --file input.txt  --model \"" + pretrained_model + "\"";
              
                pythonCmd("predict.py", args);
                this.Cursor = Cursors.Arrow;
                tbOutput.Text = cmdOutput;
            }
            catch (Exception err)
            {

            }
        }

        private void LoadPretained(object sender, RoutedEventArgs e)
        {

            var ofd = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = Directory.GetCurrentDirectory(),
                RestoreDirectory = true,
                Title = "Choose pretrained model",
                Filter = "sav files (*.sav)|*.sav|All files (*.*)|*.*"
            };
            var result = ofd.ShowDialog();
            if (result == false) return;
            pretrained_model = ofd.FileName;
            lb_pretrained_model.Content = pretrained_model.Split('\\').Last();
            lb_pretrained_model.Visibility = Visibility.Visible;
        }
    }
}
